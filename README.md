# lwd-bootstrap

> _Small wrapper around Tyxml-Lwd and Twitter's Bootstrap_

Library providing “an HTML monoid” with Bootstrap constructs.

## Install

```sh
opam exec -- opam install --with-test --with-doc lwd-bootstrap.opam
```

