include Lwd

let ( ** ) = pair
let split_var v = (get v, set v)
let bind_var : 'a var -> f:('a -> 'b t) -> 'b t = fun v ~f -> bind ~f (get v)
let update vv ~f = set vv (f (peek vv))
let set_if_different var v ~equal = if equal (peek var) v then () else set var v

module Bidirectional = struct
  type 'a t = { lwd : 'a Lwd.t; set : 'a -> unit }

  let make lwd set = { lwd; set }
  let of_var v = make (Lwd.get v) (Lwd.set v)
  let get v = v.lwd
  let set v x = v.set x

  let convert { lwd; set } f t =
    { lwd = map lwd ~f; set = (fun x -> set (t x)) }

  let bind v ~f = bind (get v) ~f
  let map v ~f = map (get v) ~f
end

module Subvar = struct
  type _ t =
    | V : {
        var : 'a var;
        projection : 'a -> 'b;
        update : 'a -> 'b -> 'a;
      }
        -> 'b t

  let of_var var projection update = V { var; projection; update }

  let set vv x =
    match vv with V { var; update; _ } -> set var (update (peek var) x)

  let peek vv =
    match vv with V { var; projection; _ } -> peek var |> projection

  let get vv =
    match vv with V { var; projection; _ } -> get var |> map ~f:projection

  let bind vv ~f = bind (get vv) ~f
  let update vv ~f = set vv (f (peek vv))
end

(* A couple of renamings to make error messages nicer: *)
type 'a lwd = 'a Lwd.t
type 'a lwd_seq = 'a Lwd_seq.t
type 'a lwd_table = 'a Lwd_table.t

module Table = struct
  include Lwd_table

  let append = append'

  let concat_map ~map table : _ Lwd.t =
    Lwd.join (Lwd_table.map_reduce map Lwd_seq.lwd_monoid table)

  let list_document table : _ List.t Lwd.t =
    Lwd.map
      (concat_map ~map:(fun _ x -> Lwd.return @@ Lwd_seq.element x) table)
      ~f:Lwd_seq.to_list

  let count table filter =
    let monoid = (0, ( + )) in
    let map _ op = if filter op then 1 else 0 in
    map_reduce map monoid table

  let sort_extract_transform :
      'a lwd_table ->
      compare:('a -> 'a -> int) ->
      extract:('a array -> 'b array) ->
      transform:('b -> 'c lwd_seq lwd) ->
      reduce:('c lwd_seq lwd -> 'c lwd_seq lwd -> 'c lwd_seq lwd) ->
      'c lwd_seq lwd =
   fun table ~compare ~extract ~transform ~reduce ->
    let sorted =
      let monoid =
        ( [||],
          fun la lb ->
            let a = Array.append la lb in
            Array.sort compare a;
            a )
      in
      Lwd_table.map_reduce (fun _ op -> [| op |]) monoid table
    in
    let extracted = Lwd.map sorted ~f:extract in
    let finished =
      let empty () = Lwd.pure Lwd_seq.empty in
      Lwd.bind extracted ~f:(fun arr ->
          Array.fold_left (fun l v -> reduce l (transform v)) (empty ()) arr)
    in
    finished

  let fold t ~init ~f =
    let rec go acc = function
      | None -> acc
      | Some row -> (
          match get row with None -> acc | Some x -> go (f acc x) (next row))
    in
    go init (first t)

  let iter_rows t ~f =
    let rec go = function
      | None -> ()
      | Some s ->
          let prepare_next = next s in
          f s;
          go prepare_next
    in
    go (first t)

  let filter table ~f =
    iter_rows table ~f:(fun row ->
        match get row with
        | Some s -> if not (f s) then remove row else ()
        | None -> ())

  let find_map t ~f =
    let monoid =
      ( None,
        function
        | Some s -> fun _ -> Some s
        | None -> ( function Some s -> Some s | None -> None) )
    in
    let map_f _row x = f x in
    map_reduce map_f monoid t

  let find t ~f =
    let monoid =
      ( None,
        function
        | Some s -> fun _ -> Some s
        | None -> ( function Some s -> Some s | None -> None) )
    in
    let map_f _row x = if f x then Some x else None in
    map_reduce map_f monoid t

  module Lwt = struct
    open Lwt.Infix

    let find_map (type a) t ~f =
      let exception Found of a in
      Lwt.catch
        (fun () ->
          fold t ~init:Lwt.return_none ~f:(fun pnone x ->
              pnone >>= fun none ->
              f x >>= function
              | Some x -> Lwt.fail (Found x)
              | None -> Lwt.return none))
        (function Found x -> Lwt.return_some x | e -> Lwt.fail e)

    let find x ~f =
      find_map x ~f:(fun x ->
          f x >>= function
          | true -> Lwt.return_some x
          | false -> Lwt.return_none)
  end
end

module Sequence = Lwd_seq

(** Experimental: attempt at variables shared over whole application redraws.
    This might be a bad idea. *)
module Global_var = struct
  module String = struct
    let _all : (string, string var) Hashtbl.t = Hashtbl.create 42

    let get ?(fresh = fun () -> "") id =
      match Hashtbl.find _all id with
      | v -> v
      | exception _ ->
          let v = var (fresh ()) in
          Hashtbl.add _all id v;
          v

    let clear id = Hashtbl.remove _all id
  end
end
