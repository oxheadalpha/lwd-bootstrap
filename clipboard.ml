(** Access the system clipboard. *)

(** Copy the contents of a (visible) text-area or input. *)
let input_element_to_clipboard ~id =
  let open Js_of_ocaml in
  let open Dom_html in
  let open Js in
  Format.eprintf "getting element by Id for %S\n%!" id;
  match getElementById_opt id with
  | None ->
      Format.eprintf "getElementByIdOpt returns None for %S\n%!" id;
      ()
  | exception e ->
      Format.eprintf "getElementByIdOpt returns Exn: %s for %S\n%!"
        (Printexc.to_string e) id;
      ()
  | Some ele -> (
      let of_typed_element ele =
        (* Cf. https://www.w3schools.com/howto/howto_js_copy_clipboard.asp *)
        ele##select;
        ele##.selectionStart := 0;
        ele##.selectionEnd := 9999999;
        Format.eprintf "Value: %S\n%!" (Js.to_string ele##.value);
        let nav = window##.navigator in
        Format.eprintf "Navigator: %S %S\n%!"
          (Js.to_string nav##.appName)
          (Js.to_string nav##.appVersion);
        let clipboard = Unsafe.get nav (Js.string "clipboard") in
        let () =
          Unsafe.meth_call clipboard "writeText" [| Unsafe.inject ele##.value |]
        in
        ()
      in
      match tagged ele with
      | Textarea ele -> of_typed_element ele
      | Input ele -> of_typed_element ele
      | _ ->
          (* The fallback, older school: *)
          Format.eprintf "getElementByIdOpt returns not an input for %S\n%!" id;
          let () = Unsafe.meth_call ele "select" [||] in
          let () =
            document##execCommand (Js.string "selectAll") (Js.bool true)
              (Js.Opt.option None)
          in
          Unsafe.meth_call document "execCommand" [| Unsafe.inject "copy" |])
