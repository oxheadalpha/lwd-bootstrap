(** Return a list, elements mapped through ~map, with elements before the last
    separated with ~sep, and with the last element separated with ~last_sep.
    Note that ~last_sep must include ~sep if you want a genuine Oxford comma --
    but then you'll get it in two-element lists too*)
let rec oxfordize_list l ~map ~sep ~last_sep =
  match l with
  | [] -> []
  | [ one ] -> [ map one ]
  | [ one; two ] -> [ map one; last_sep (); map two ]
  | one :: more -> map one :: sep () :: oxfordize_list more ~map ~sep ~last_sep

let ellipsize_string ?(ellipsis = " …") s ~max_length =
  if String.length s <= max_length then s
  else String.sub s 0 max_length ^ ellipsis

let string_summary ?(threshold = 25) ?(left = 10) ?(right = 10) bytes =
  match String.length bytes with
  | m when m < threshold -> bytes
  | m ->
      Printf.sprintf "%s…%s" (String.sub bytes 0 left)
        (String.sub bytes (m - right) right)
