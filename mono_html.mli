(** {1 Integration of {!Tyxml_lwd.Html} Into An HTML Monoid} *)

module H5 = Tyxml_lwd.Html

type 'a t = 'a H5.elt

(** {1 Basic Monoid} *)

val empty : unit -> 'a t
val ( % ) : 'a t -> 'a t -> 'a t

(** {1 HTML Constructs} *)

val t : string -> [> Html_types.txt ] t
val ( %% ) : ([> Html_types.txt ] as 'a) t -> 'a t -> 'a t
val list : 'a t list -> 'a t
val concat_map : f:('a -> 'b t) -> sep:(unit -> 'b t) -> 'a list -> 'b t
val parens : ([> Html_types.txt ] as 'a) t -> 'a t

val hr :
  ?a:[< Html_types.hr_attrib ] H5.attrib list -> unit -> [> Html_types.hr ] t

val br :
  ?a:[< Html_types.br_attrib ] H5.attrib list -> unit -> [> Html_types.br ] t

val p :
  ?a:[< Html_types.p_attrib ] H5.attrib list ->
  [< Html_types.p_content_fun ] t ->
  [> Html_types.p ] t

val i :
  ?a:[< Html_types.i_attrib ] H5.attrib list ->
  [< Html_types.i_content_fun ] t ->
  [> Html_types.i ] t

val b :
  ?a:[< Html_types.b_attrib ] H5.attrib list ->
  [< Html_types.b_content_fun ] t ->
  [> Html_types.b ] t

val em :
  ?a:[< Html_types.em_attrib ] H5.attrib list ->
  [< Html_types.em_content_fun ] t ->
  [> Html_types.em ] t

val code :
  ?a:[< Html_types.code_attrib ] H5.attrib list ->
  [< Html_types.code_content_fun ] t ->
  [> Html_types.code ] t

val span :
  ?a:[< Html_types.span_attrib ] H5.attrib list ->
  [< Html_types.span_content_fun ] t ->
  [> Html_types.span ] t

val sub :
  ?a:[< Html_types.sub_attrib ] H5.attrib list ->
  [< Html_types.sub_content_fun ] t ->
  [> Html_types.sub ] t

val sup :
  ?a:[< Html_types.sup_attrib ] H5.attrib list ->
  [< Html_types.sup_content_fun ] t ->
  [> Html_types.sup ] t

val small :
  ?a:[< Html_types.small_attrib ] H5.attrib list ->
  [< Html_types.small_content_fun ] t ->
  [> Html_types.small ] t

val strong :
  ?a:[< Html_types.strong_attrib ] H5.attrib list ->
  [< Html_types.strong_content_fun ] t ->
  [> Html_types.strong ] t

val abbr :
  ?a:[< Html_types.abbr_attrib ] H5.attrib list ->
  [< Html_types.abbr_content_fun ] t ->
  [> Html_types.abbr ] t

val a :
  ?a:[< Html_types.a_attrib ] H5.attrib list -> 'a t -> [> 'a Html_types.a ] t

val div :
  ?a:[< Html_types.div_attrib ] H5.attrib list ->
  [< Html_types.div_content_fun ] t ->
  [> Html_types.div ] t

val pre :
  ?a:[< Html_types.pre_attrib ] H5.attrib list ->
  [< Html_types.pre_content_fun ] t ->
  [> Html_types.pre ] t

val h1 :
  ?a:[< Html_types.h1_attrib ] H5.attrib list ->
  [< Html_types.h1_content_fun ] t ->
  [> Html_types.h1 ] t

val h2 :
  ?a:[< Html_types.h2_attrib ] H5.attrib list ->
  [< Html_types.h2_content_fun ] t ->
  [> Html_types.h2 ] t

val h3 :
  ?a:[< Html_types.h3_attrib ] H5.attrib list ->
  [< Html_types.h3_content_fun ] t ->
  [> Html_types.h3 ] t

val h4 :
  ?a:[< Html_types.h4_attrib ] H5.attrib list ->
  [< Html_types.h4_content_fun ] t ->
  [> Html_types.h4 ] t

val h5 :
  ?a:[< Html_types.h5_attrib ] H5.attrib list ->
  [< Html_types.h5_content_fun ] t ->
  [> Html_types.h5 ] t

val h6 :
  ?a:[< Html_types.h6_attrib ] H5.attrib list ->
  [< Html_types.h6_content_fun ] t ->
  [> Html_types.h6 ] t

val tr :
  ?a:[< Html_types.tr_attrib ] H5.attrib list ->
  [< Html_types.tr_content_fun ] t ->
  [> Html_types.tr ] t

val td :
  ?a:[< Html_types.td_attrib ] H5.attrib list ->
  [< Html_types.td_content_fun ] t ->
  [> Html_types.td ] t

val th :
  ?a:[< Html_types.th_attrib ] H5.attrib list ->
  [< Html_types.th_content_fun ] t ->
  [> Html_types.th ] t

val blockquote :
  ?a:[< Html_types.blockquote_attrib ] H5.attrib list ->
  [< Html_types.blockquote_content_fun ] t ->
  [> Html_types.blockquote ] t

val nav :
  ?a:[< Html_types.nav_attrib ] H5.attrib list ->
  [< Html_types.nav_content_fun ] t ->
  [> Html_types.nav ] t

val classes : string list -> [> `Class ] H5.attrib
val style : string -> [> `Style_Attr ] H5.attrib
val id_string : string -> [> `Id ] H5.attrib
val it : string -> [> Html_types.i ] t
val bt : string -> [> Html_types.b ] t
val ct : string -> [> Html_types.code ] t

val link :
  target:string ->
  ?a:[< Html_types.a_attrib > `Href ] H5.attrib list ->
  'a t ->
  [> 'a Html_types.a ] t

val url :
  ?a:[< Html_types.a_attrib > `Href ] H5.attrib list ->
  (string -> 'a t) ->
  string ->
  [> 'a Html_types.a ] t

val abbreviation :
  string -> [< Html_types.abbr_content_fun ] t -> [> Html_types.abbr ] t

val abbreviation_with_toggle :
  ?state:[ `Abbreviated | `Expanded ] Lwd.var ->
  string ->
  abbreviated:[< Html_types.abbr_content_fun ] t ->
  expanded:[< Html_types.span_content_fun ] t ->
  [> `Abbr | `Span ] t

val button :
  ?a:[< Html_types.button_attrib > `OnClick ] H5.attrib list ->
  action:(unit -> unit) ->
  [< Html_types.button_content_fun ] t ->
  [> Html_types.button ] t

val onclick_action : (unit -> unit) -> [> `OnClick ] H5.attrib

val itemize :
  ?numbered:bool ->
  ?a_ul:[< Html_types.ul_attrib ] H5.attrib list ->
  ?a_li:[< Html_types.li_attrib ] H5.attrib list ->
  [< Html_types.li_content_fun ] t list ->
  [> `Ol | `Ul ] t

val details :
  summary:[< Html_types.summary_content_fun ] t ->
  [< Html_types.details_content_fun ] t ->
  [> Html_types.details ] t

val input_bidirectional :
  ?a:[< Html_types.input_attrib > `OnInput `Value ] H5.attrib list ->
  string Reactive.Bidirectional.t ->
  [> Html_types.input ] t

module Unsafe : sig
  val html_string : element_name:string -> string -> 'a t
  val span_string : string -> 'a t
  val div_string : string -> 'a t
end
